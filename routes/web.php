<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/', function () {
	return view( 'welcome' );
} );

Auth::routes();

Route::get( '/home', 'HomeController@index' )
     ->name( 'home' );

//Auth
Route::group( [
	'middleware' => [
		'auth', 'isAdmin'
	]
], function () {
	Route::group( [ 'prefix' => '/user/setting' ], function () {
		
		Route::get( '', [
			'uses' => 'AuthController@index'
		] )
		     ->name( 'auth.index' );
		
		// For Datatables
		Route::get( '/ajax/anyData', 'AuthController@anyData' )
		     ->name( 'auth.data' );
		
		// For Form
		Route::group( [
			'prefix' => '/form'
		], function () {
			
			Route::get( '/create', [
				'uses' => 'AuthController@create'
			] )
			     ->name( 'auth.create' );;
			
			Route::get( '/update/{id}', [
				'uses' => 'AuthController@edit'
			] )
			     ->where( 'id', '[0-9]+' )
			     ->name( 'auth.edit' );;
			
			Route::get( '/detail/{id}', [
				'uses' => 'AuthController@show'
			] )
			     ->where( 'id', '[0-9]+' )
			     ->name( 'auth.show' );;
		} );
		
		// For Action
		Route::group( [
			'prefix' => '/action'
		], function () {
			Route::post( '/insert', [
				'uses' => 'AuthController@store'
			] )
			     ->name( 'auth.store' );
			
			Route::post( '/update/{id}', [
				'uses' => 'AuthController@update'
			] )
			     ->name( 'auth.update' );
			
			Route::get( '/remove/{id}', [
				'uses' => 'AuthController@destroy'
			] )
			     ->name( 'auth.destroy' );
		} );
	} );
} );