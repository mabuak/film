<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = new Role();
        $roleAdmin->name = 'admin';
        $roleAdmin->description = 'A Administrator';
        $roleAdmin->save();
	
	    $roleUser = new Role();
	    $roleUser->name = 'user';
        $roleUser->description = 'A Users';
        $roleUser->save();
    }
}
