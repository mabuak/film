<?php

use App\Models\Film;
use Illuminate\Database\Seeder;

class FilmGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $dataDb       = new Film();
	    $dataDb->name = 'Logan';
	    $dataDb->slug = str_slug('Logan');
	    $dataDb->description = 'In the near future, a weary Logan cares for an ailing Professor X, somewhere on the Mexican border. However, Logan\'s attempts to hide from the world, and his legacy, are upended when a young mutant arrives, pursued by dark forces.';
	    $dataDb->release_date = date('Y-m-d', strtotime('2017-02-28'));
	    $dataDb->rating = 5;
	    $dataDb->ticket_price = 100.1;
	    $dataDb->country = 'Usa';
	    $dataDb->images = 'https://images-na.ssl-images-amazon.com/images/M/MV5BMjQwODQwNTg4OV5BMl5BanBnXkFtZTgwMTk4MTAzMjI@._V1_UX182_CR0,0,182,268_AL_.jpg';
	    $dataDb->save();
	
	    $dataDb->genre()->sync([1, 2]);
	    $dataDb->comment()->create(['name' => 'rama', 'comment' => 'Good']);
	    
	    $dataDb       = new Film();
	    $dataDb->name = 'Transformers: The Last Knight';
	    $dataDb->slug = str_slug('Transformers: The Last Knight');
	    $dataDb->description = 'Autobots and Decepticons are at war, with humans on the sidelines. Optimus Prime is gone. The key to saving our future lies buried in the secrets of the past, in the hidden history of Transformers on Earth.';
	    $dataDb->release_date = date('Y-m-d', strtotime('2017-06-21'));
	    $dataDb->rating = 4;
	    $dataDb->ticket_price = 50.1;
	    $dataDb->country = 'Usa';
	    $dataDb->images = 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTk3OTI3MDk4N15BMl5BanBnXkFtZTgwNDg2ODIyMjI@._V1_UY268_CR0,0,182,268_AL_.jpg';
	    $dataDb->save();
	
	    $dataDb->genre()->sync([1, 2, 3]);
	    $dataDb->comment()->create(['name' => 'rama', 'comment' => 'Good']);
	
	    $dataDb       = new Film();
	    $dataDb->name = 'Despicable Me 3';
	    $dataDb->slug = str_slug('Despicable Me 3');
	    $dataDb->description = 'Gru meets his long-lost charming, cheerful, and more successful twin brother Dru who wants to team up with him for one last criminal heist.';
	    $dataDb->release_date = date('Y-m-d', strtotime('2017-06-29'));
	    $dataDb->rating = 4;
	    $dataDb->ticket_price = 50.1;
	    $dataDb->country = 'Usa';
	    $dataDb->images = 'https://images-na.ssl-images-amazon.com/images/M/MV5BNjUyNzQ2MTg3Ml5BMl5BanBnXkFtZTgwNzE4NDM3MTI@._V1_UX182_CR0,0,182,268_AL_.jpg';
	    $dataDb->save();
	
	    $dataDb->genre()->sync([3, 4, 5]);
	    $dataDb->comment()->create(['name' => 'rama', 'comment' => 'Good']);
    }
}
