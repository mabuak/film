<?php

use App\Models\Genre;
use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $dataDb       = new Genre();
	    $dataDb->name = 'Action';
	    $dataDb->slug = str_slug('Action');
	    $dataDb->save();
	    
	    $dataDb       = new Genre();
	    $dataDb->name = 'Adventure';
	    $dataDb->slug = str_slug('Adventure');
	    $dataDb->save();
	    
	    $dataDb       = new Genre();
	    $dataDb->name = 'Animation';
	    $dataDb->slug = str_slug('Animation');
	    $dataDb->save();
	
	    $dataDb       = new Genre();
	    $dataDb->name = 'Comedy';
	    $dataDb->slug = str_slug('Comedy');
	    $dataDb->save();
	
	    $dataDb       = new Genre();
	    $dataDb->name = 'Crime';
	    $dataDb->slug = str_slug('Crime');
	    $dataDb->save();
    }
    
}
