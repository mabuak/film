<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$roleAdmin = Role::where( 'name', 'admin' )
		                 ->first();
		$roleUser  = Role::where( 'name', 'user' )
		                 ->first();
		
		$userAdmin           = new User();
		$userAdmin->name     = 'Admin';
		$userAdmin->email    = 'admin@admin.com';
		$userAdmin->password = bcrypt( 'password' );
		$userAdmin->save();
		$userAdmin->roles()
		          ->attach( $roleAdmin );
		
		$user           = new User();
		$user->name     = 'User';
		$user->email    = 'user@admin.com';
		$user->password = bcrypt( 'password' );
		$user->save();
		$user->roles()
		     ->attach( $roleUser );
		
	}
}
