<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class IsAdmin {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		if ( Auth::user() && Auth::user()
		                         ->authorizeRoles( [ 'admin' ] ) ) {
			return $next( $request );
		}
		
		Session::flash( 'failed', 'Access Denied' );
		
		return redirect( '/film' );
	}
}
