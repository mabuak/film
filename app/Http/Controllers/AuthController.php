<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthSetting\createRequest;
use App\Http\Requests\AuthSetting\updateRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class AuthController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view( 'auth.index' );
	}
	
	/**
	 * Datatables Ajax Data
	 *
	 * @return mixed
	 */
	public function anyData( Request $request ) {
		
		$dataDb = User::select( 'users.id', 'users.name', 'email', 'users.created_at', 'users.updated_at' )
		              ->with( 'roles' );
		
		return DataTables::eloquent( $dataDb )
		                 ->addColumn( 'action', function ( $dataDb ) {
			                 return '<a href="' . action( 'AuthController@edit', $dataDb->id ) . '" id="tooltip" title="Edit"><span class="label label-warning label-sm"><i class="fa fa-edit"></i></span></a> <a href="#" data-value="' . $dataDb->name . '" data-href="' . action( 'AuthController@destroy', $dataDb->id ) . '" id="tooltip" data-title="Delete" data-toggle="modal" data-target="#delete" title="Delete"><span class="label label-danger label-sm"><i class="fa fa-trash-o"></i></span></a>';
		                 } )
		                 ->addColumn( 'checkbox', function ( $dataDb ) {
			                 return $dataDb->id;
		                 } )
		                 ->addColumn( 'roles', function ( User $dataDb ) {
			                 return $dataDb->roles->map( function ( $roles ) {
				                 return $roles->name;
			                 } )
			                                      ->implode( '<br>' );
		                 } )
		                 ->make( true );
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view( 'auth.create', array( 'roles' => Role::get() ) );
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param createRequest|Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( createRequest $request ) {
		
		$user = User::create( [
			'name'     => $request->name,
			'email'    => $request->email,
			'password' => bcrypt( $request->password ),
		] );
		
		$user->roles()
		     ->attach( Role::find( $request->role ) );
		
		Session::flash( 'success', 'You have successfully add the new record' );
		
		return redirect()->route( 'auth.index' );
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		
		$dataDb = User::findOrFail( $id );
		
		return view( 'auth.update', array( 'dataDb' => $dataDb, 'roles' => Role::get() ) );
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param updateRequest|Request $request
	 * @param  int                  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( updateRequest $request, $id ) {
		
		$dataDb        = User::findOrFail( $id );
		$dataDb->name  = $request->name;
		$dataDb->email = $request->email;
		
		if ( $request->password != null ) {
			$dataDb->password = bcrypt( $request->password );
		}
		
		$dataDb->save();
		
		Session::flash( 'success', 'You have successfully change the record' );
		
		return redirect()->route( 'auth.index' );
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		
		#This User Cannot Remove Them Self
		if ( $id == Auth::id() ) {
			Session::flash( 'failed', 'Cannot Remove Your Own User' );
			
			return redirect()->back();
		}
		
		$dataDb = User::find( $id );
		
		if ( $dataDb->isEmpty ) {
			Session::flash( 'failed', 'Not Found!!' );
			
			return redirect()->back();
		}
		
		$dataDb->delete();
		
		Session::flash( 'success', 'You have successfully remove the record ' );
		
		return redirect()->back();
		
	}
}
