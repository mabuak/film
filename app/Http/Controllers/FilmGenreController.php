<?php

namespace App\Http\Controllers;

use App\Models\FilmGenre;
use Illuminate\Http\Request;

class FilmGenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FilmGenre  $filmGenre
     * @return \Illuminate\Http\Response
     */
    public function show(FilmGenre $filmGenre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FilmGenre  $filmGenre
     * @return \Illuminate\Http\Response
     */
    public function edit(FilmGenre $filmGenre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FilmGenre  $filmGenre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FilmGenre $filmGenre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FilmGenre  $filmGenre
     * @return \Illuminate\Http\Response
     */
    public function destroy(FilmGenre $filmGenre)
    {
        //
    }
}
