﻿<div class="panel-footer">
    <input type="hidden" value="{{old('previousUrl') ? old('previousUrl') : url()->previous()}}" name="previousUrl">
    <a href="{{old('previousUrl') ? old('previousUrl') : url()->previous()}}" class="btn btn-flat btn-default btn-sm"><i class="fa fa-reply"></i> Batal</a>
    
    <div class="pull-right">
        <img id="loading-spinner" src="{{url('images/spinner.gif')}}" alt="" style="float: left;margin-top: 4px;margin-right: 10px; display: none">
        <button type="submit" class="btn btn-flat btn-success btn-sm" id="submit"><i class="fa fa-save"></i> Simpan</button>
    </div>
    
    <div class="clearfix"></div>
</div>