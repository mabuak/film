﻿@if(session()->has('success'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        
        <strong>{!! session()->get('success') !!}</strong>
    </div>
@endif

@if(session()->has('failed'))
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        
        <strong>{!! session()->get('failed') !!}</strong>
    </div>
@endif

@if($errors->all())
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        
        <strong><i class="fa fa-warning"></i> Warning: Please check the form carefully for errors!</strong>
    </div>
@endif