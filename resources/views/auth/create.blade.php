@extends('layouts.app')

@push('css')
    <!-- Select 2 -->
    <link rel="stylesheet" href="{{url('plugins/select2/css/select2.css')}}">
    <link rel="stylesheet" href="{{url('plugins/select2/css/select2-bootstrap.css')}}">
@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('layouts.flash')
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="fa fa-tasks"></span> Pengaturan Pengguna
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('auth.store') }}">
                        {{ csrf_field() }}
                        
                        <div class="panel-body">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nama <span style="color: red">*</span></label>
                                
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control input-sm" name="name" value="{{ old('name') }}" autofocus>
                                    
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Alamat E-Mail <span style="color: red">*</span></label>
                                
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control input-sm" name="email" value="{{ old('email') }}">
                                    
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Kata Sandi <span style="color: red">*</span></label>
                                
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control input-sm" name="password">
                                    
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Konfirmasi Kata Sandi <span style="color: red">*</span></label>
                                
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control input-sm" name="password_confirmation">
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Tugas <span style="color: red">*</span></label>
                                
                                <div class="col-md-6">
                                    <div class="input-group input-group-sm">
                                        <select class="form-control input-sm select_2" name="role" id="role" data-placeholder="Select" style="width: 100%">
                                            <option value=""></option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}" @if(old('role') == $role->id) selected @endif>{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('role'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        
                        </div>
                        @include('layouts.form-footer')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <!-- Select 2 -->
    <script src="{{url('plugins/select2/js/select2.full.js')}}"></script>
    
    <script>
        $('.select_2').select2({
            theme            : "bootstrap",
            placeholder      : "Select",
            allowClear       : true,
            width            : '100%',
            containerCssClass: ':all:',
        });
    </script>
@endpush