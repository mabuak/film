@extends('layouts.app')

@push('css')
    <link href="{{ asset('plugins/DataTables/datatables.css') }}" rel="stylesheet">
@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('layouts.flash')
    
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="fa fa-tasks"></span> Pengaturan Pengguna
                    </div>
        
                    <div class="panel-body">
                        <a href="{{url('user/setting/form/create')}}" class="btn btn-flat btn-success btn-sm">Tambah Baru</a>
                        <hr>
                        <table class="table table-striped table-bordered table-hover table-condensed" id="users-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Tugas</th>
                                <th>Dibuat Di</th>
                                <th>Dirubah Di</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('javascript')
    
    <!-- DataTables -->
    <script src="{{url('plugins/DataTables/datatables.min.js')}}"></script>
    <script src="{{url('plugins/DataTables/Responsive-2.2.0/js/responsive.bootstrap.min.js')}}"></script>
    <script src="{{url('plugins/DataTables/FixedHeader-3.1.3/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{url('plugins/DataTables/Buttons-1.4.2/js/buttons.bootstrap.js')}}"></script>
    <script src="{{url('plugins/DataTables/Buttons-1.4.2/js/buttons.colVis.js')}}"></script>
    
    <script>
        var oTable = $('#users-table').DataTable({
            aaSorting     : [[0, 'desc']],
            iDisplayLength: 25,
            stateSave     : true,
            processing    : true,
            serverSide    : true,
            pagingType    : "full_numbers",
            dom           : "<'row'<'col-sm-2'B><'col-sm-3'l><'col-sm-4'<'toolbar'>><'col-sm-3'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            ajax          : {
                url     : '{!! route('auth.data') !!}',
                dataType: 'json'
            },
            language: {
                url: "{{asset('plugins/DataTables/Indonesian.json')}}"
            },


            columns: [
                {data: 'id', name: 'id', visible: true},
                {data: 'name', name: 'name', visible: true},
                {data: 'email', name: 'email', visible: true},
                {data: 'roles', name: 'roles.name', visible: true},
                {data: 'created_at', name: 'created_at', visible: false},
                {data: 'updated_at', name: 'updated_at', visible: false},
                {
                    data           : 'action', name: 'action', orderable: false, searchable: false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        $("a", nTd).tooltip({container: 'body'});
                    }
                }
            ],
            buttons: [
                {
                    extend : 'colvis',
                    text   : '<i class="fa fa-columns"></i> Kolom',
                    columns: '1, 2, 3, 4, 5'
                }
            ]
        });
    </script>
    
    <script src="{{url('plugins/select2/js/select2.js')}}"></script>
    
    <script type="text/javascript">
        $(function () {
            $('select').select2({
                theme                  : "bootstrap",
                placeholder            : "Select",
                allowClear             : false,
                minimumResultsForSearch: Infinity
            });
        })
    
    </script>
@endpush